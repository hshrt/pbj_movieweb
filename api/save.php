<?php
	$dir = "../history";
	if (!file_exists($dir)) {
		mkdir($dir, 0777, true);
	}
	if(isset($_REQUEST['filename-input']) && isset($_REQUEST['data'])) {
		$filename = $_REQUEST['filename-input'].".mld";
		$i = 0;
    		while (file_exists($dir . "/" . $filename)) {
			$i++;
			$filename = $_REQUEST['filename-input'] . "_" . $i . ".mld";
    		}
		$fh = fopen($dir . "/" . $filename, 'w') or die("Can't create file");
		fwrite($fh, $_REQUEST['data']);
		fclose($fh);
	}
	echo "{}";
?>
