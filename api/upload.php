<?php

$directorySeparator = '/';
$storeFolder = '../uploads';

if (!empty($_FILES)) {
	$tempFile = $_FILES['file']['tmp_name'];
	$targetPath = dirname(__FILE__) . $directorySeparator . $storeFolder . $directorySeparator;
	$fileSuf = $_FILES['file']['name'];
	if (!empty($_POST) && !empty($_POST['fullPath'])) {
		$fileSuf = $_POST['fullPath'];
		if (strpos($fileSuf, '/') !== false) {
			$folder = substr($fileSuf, 0, strrpos($fileSuf, '/'));
			$mkDir = $targetPath . $folder . $directorySeparator;
		} else {
			$mkDir = $targetPath;
		}
		if (!file_exists($mkDir)) {
			mkdir($mkDir, 0777, true);
		}
	} else {
		$mkDir = $targetPath;
		if (!file_exists($mkDir)) {
			mkdir($mkDir, 0777, true);
		}
	}
	$targetFile = $targetPath . $fileSuf;
	move_uploaded_file($tempFile, $targetFile);
	print_r( $targetFile );
}
?>
