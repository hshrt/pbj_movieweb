<?php
	$upload = "../uploads";
	$dir = "../movies";
	if (!file_exists($dir)) {
		mkdir($dir, 0777, true);
	}
	foreach ($_REQUEST as $key => $value) {
		if (preg_match("(^radio-)", $key, $match)) {
			$id = preg_replace("(^radio-)", "", $key);
			$path = $_REQUEST["path-" . $id];
			$movie = $_REQUEST["input-" . $id];
			$radio = $value;
			$file = "";
			$ext = "";

			switch ($radio) {
				case "file-option-none":
					break;
				case "file-option-small":
					$file = "small.png";
					break;
				case "file-option-big-chi":
					$file = "big_c.jpg";
					break;
				case "file-option-big-eng":
					$file = "big_e.jpg";
					break;
			}
			if (strlen($file) != 0 && strlen($movie) != 0) {
				$newDir = $dir . "/" . $movie . "/";
				$newPath = $newDir . $file;
				if (!file_exists($newDir)) {
					mkdir($newDir, 0777, true);
				}
				$src = $upload . "/" . $path;

				copy($src, $newPath);
			}
		}
	}
	echo "{}";
?>
