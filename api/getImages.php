<?php

$scan = "/../uploads";

class FileObject {
	public $path;
	public $name;
	public $isDir;
	public $ext;
	public $child = [];

	function __construct($dir, $file) {
		$this->path = "$dir/$file";
		$this->name = $file;
		$this->isDir = check_is_dir(getcwd() . "/" . $this->path);
		if ( $this->isDir ) {
			$this->ext = "";
		} else {
			$this->ext = substr($this->name, strrpos($this->name, ".") + 1);
		}
	}
}

function getFiles($dir, $ext = array()) {
	if (substr($dir, -1) == "/") {
		$dir = substr($dir, 0, strlen($dir) - 1);
	}
	return _getFiles($dir, $ext);
}

function _getFiles($dir, $ext = array()) {
	if ( function_exists("scandir") ) {
		$items = scandir($dir);
	} else {
		$items = php4_scandir($dir);
	}
	natcasesort($items);

	$files = $dirs = array();
	foreach ($items as $f) {
		if ( check_is_dir("$dir/$f") ) {
			$dirs[] = $f;
		} else {
			$files[] = $f;
		}
	}
	$items = array_merge($dirs, $files);

	$returnObjs = array();
	foreach (array_keys($items) as $key) {
		$filename = $items[$key];
		if ( $filename != "." && $filename != ".." ) {
			$obj = new FileObject(str_replace(getcwd() . "/", "", $dir), $filename);
			if ( !$obj->isDir ) {
				if ( !empty($ext) && !in_array($obj->ext, $ext) ) {
					unset($items[$key]);
				} else {
					$returnObjs[] = $obj;
				}
			} else {
				$obj->child = _getFiles(getcwd() . "/" . $obj->path, $ext);
				$returnObjs[] = $obj;
			}
		}
	}
	return $returnObjs;
}

function php4_scandir($dir) {
	$dh = opendir($dir);
	while ( false !== ($filename = readdir($dh)) ) {
		$files[] = $filename;
	}
	sort($files);
	return($files);
}

function check_is_dir($path) {
	return is_dir($path);
}

$result = getFiles(getcwd() . $scan);

http_response_code(200);
echo json_encode($result);
