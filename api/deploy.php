<?php
	$sourceDir = "../movies/";
	$destDir = "../../cmspbj/";
	if(isset($_REQUEST['deploy']) && isset($_REQUEST['data'])) {
		$folderType = $_REQUEST['deploy'];
		$folder = "movie_other";
		switch ($folderType) {
			case "prod":
				$folder = "movie";
				break;
			case "backup":
				$folder = "movie" . date("Ym");
				break;
		}

		$data = json_decode($_REQUEST['data'], true);
		$newDir = $destDir . $folder;
		if (!file_exists($newDir)) {
			mkdir($newDir, 0777, true);
		}

		array_map('unlink', array_filter((array) array_merge(glob($newDir . "/*")))); 

		foreach ($data as $obj) {
			$no = $obj["no"];
			$movie = $obj["chi"];

			if (strlen($movie) != 0) {
				$src = $sourceDir . $movie . "/small.png";
				$newPath = $newDir . "/small" . $no . ".png";

				copy($src, $newPath);

				$src = $sourceDir . $movie . "/big_c.jpg";
				$newPath = $newDir . "/big" . $no . "_c.jpg";

				copy($src, $newPath);

				$src = $sourceDir . $movie . "/big_e.jpg";
				$newPath = $newDir . "/big" . $no . "_e.jpg";

				copy($src, $newPath);
			}
		}

		echo "{}";
	}
?>
