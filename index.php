<html>
<head>

<link href="css/dropzone.min.css" type="text/css" rel="stylesheet" />
<link href="css/index.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-3.5.1.min.js"></script>
<script src="js/dropzone.min.js"></script>
<script src="js/index.js"></script>
<script src="js/i18n.min.js"></script>
<script>
var lang = [];

var enStr = '<?php echo str_replace("\n", "\\n", file_get_contents("i18n/en.json")); ?>';
lang["en"] = i18n.create(JSON.parse(enStr));

$(document).ready(function() {
	refreshContent();
});

function refreshContent() {
	$("*[i18n]").each(function(index, value) {
		setText(value, value.getAttribute("i18n"));
	});
}

function setLocaleText(element) {
	setText(element, element.getAttribute("i18n"));
}

function setText(element, key) {
	if (element.tagName.toLowerCase() == 'input') {
		element.value = lang["en"](key);
	} else {
		element.innerHTML = lang["en"](key);
	}
}

</script>
</head>
<body>
	<div class="topnav">
		<a class="active" href="" i18n="home"></a>
		<a id="asave" i18n="save"></a>
		<a id="aload" i18n="load"></a>
		<a id="adeploy" i18n="deploy"></a>
	</div>
	<div id="title"><h1 id="title-text" i18n="title"></h1></div>
	<div id="input-box">
		<div id="my-input-list">
			<textarea></textarea>
		</div>
		<div id="my-upload">
			<form action="api/upload.php" class="dropzone" id="my-dropzone"><div id="my-dropzone-hidden"></div></form>
			<div class="hide" id="upload-bind-container">
				<button type="submit" class="btn div-right" i18n="bind" id="upload-bind"></button>
			</div>
		</div>
	</div>
	<div id="my-list"></div>
	<div class="success-popup close-parent" id="success-popup">
		<form action="api/update.php" id="success-container">
			<span class="close">&times;</span>
			<h3 i18n="uploaded-title"></h3>
			<div id="success-table"></div>
			<button type="submit" class="btn div-right" i18n="confirm"></button>
		</form>
	</div>
	<div class="save-popup close-parent" id="save-popup">
		<form action="api/save.php" id="save-container" method="POST">
			<span class="close">&times;</span>
			<h3 i18n="save"></h3>
			<div>
				<span i18n="filename"></span>
				<input type="text" id="filename-input" name="filename-input"></input>
			</div>
			<button type="submit" class="btn div-right" i18n="confirm"></button>
		</form>
	</div>
	<div class="load-popup close-parent" id="load-popup">
		<form action="api/load.php" id="load-container">
			<span class="close">&times;</span>
			<h3 i18n="load"></h3>
			<div id="load-table">
				<select id="filename-select" name="filename-select" size="8"></select>
			</div>
			<button type="submit" class="btn div-right" i18n="load"></button>
		</form>
	</div>
	<div class="deploy-popup close-parent" id="deploy-popup">
		<form action="api/deploy.php" id="deploy-container" method="POST">
			<span class="close">&times;</span>
			<h3 i18n="deploy"></h3>
			<div>
				<input type="radio" id="deploy-prod" name="deploy" value="prod">
				<label for="deploy-prod">Production</label><br>
				<input type="radio" id="deploy-backup" name="deploy" value="backup">
				<label for="deploy-backup">Backup</label><br>
				<input type="radio" id="other" name="deploy" value="other">
				<label for="other">Other</label>
			</div>
			<button type="submit" class="btn div-right" i18n="confirm"></button>
		</form>
	</div>
</body>
