var radioKey = [ "file-option-none", "file-option-small", "file-option-big-chi", "file-option-big-eng" ];
var movieList = [];
var successFiles = [];

Dropzone.options.myDropzone = {
	init: function() {
		this.on("complete", function(file) {
			if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
				displayUploaded(successFiles);
			}
		});
		this.on("success", function(file, response) {
			successFiles.push(file);
		});
		this.on("sending", function(file, xhr, data) {
			if (file.fullPath) {
				$("#my-dropzone-hidden").html('<input type="hidden" name="fullPath" value="' + file.fullPath + '" />');
			}
		});
	}
};

$(document).ready(function() {
	$("#asave").on("click", function(event) {
		var today = new Date();
		var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = today.getFullYear();

		today = yyyy + '_' + mm;
		$('#filename-input').val(today);
		$("#save-popup").show();
	});
	$("#aload").on("click", function(event) {
		$.ajax({
			type: "GET",
			url: "api/getSaves.php",
			dataType: "json",
			success: function (data) {
				checkSave(data);
				$("#load-popup").show();
			}
		});

	});
	$("#adeploy").on("click", function(event) {
		$("#deploy-popup").show();
	});

	$("#my-input-list").find("textarea").bind('input propertychange', function() {
		if (checkInput($(this).val())) {
			$("#my-input-list").find("textarea").prop("disabled", true);
		}
	});

	$(".close").on("click", function(event) {
		$(this).parentsUntil(".close-parent").parent().hide();
	});

	$("#success-container").submit(function(e) {
		e.preventDefault();
		var form = $(this)[0];
		$.ajax({
			type: form.method,
			url: form.action,
			data: $(this).serialize(),
			dataType: "json",
			success: function (data) {
				$(".close").each(function (i, obj) {
					$(obj).trigger("click");
				});
				checkInput($("#my-input-list").find("textarea").val());
			}
		});
	});

	$("#save-container").submit(function(e) {
		e.preventDefault();
		var form = $(this)[0];
		var data = $(this).serializeArray();
		data.push({name: "data", value: $("#my-input-list").find("textarea").val()});
		$.ajax({
			type: form.method,
			url: form.action,
			data: $.param(data),
			dataType: "json",
			success: function (data) {
				$(".close").each(function (i, obj) {
					$(obj).trigger("click");
				});
			}
		});
	});

	$("#load-container").submit(function(e) {
		e.preventDefault();
		var form = $(this)[0];
		$.ajax({
			type: form.method,
			url: form.action,
			data: $(this).serialize(),
			dataType: "text",
			success: function (data) {
				$(".close").each(function (i, obj) {
					$(obj).trigger("click");
				});
				$("#my-input-list").find("textarea").val(data);
				checkInput($("#my-input-list").find("textarea").val());
			}
		});
	});

	$("#deploy-container").submit(function(e) {
		e.preventDefault();
		var form = $(this)[0];
		var data = $(this).serializeArray();
		data.push({name: "data", value: JSON.stringify(movieList)});
		$.ajax({
			type: form.method,
			url: form.action,
			data: $.param(data),
			dataType: "json",
			success: function (data) {
				$(".close").each(function (i, obj) {
					$(obj).trigger("click");
				});
			}
		});
	});

	$("#upload-bind").on("click", function(event) {
		displayUploaded(successFiles);
	});
});

function displayUploaded(files) {
	$("#upload-bind-container").show();
	if (files.length > 0) {
		var fragment = document.createDocumentFragment();
		var newFiles = [];
		for (var key in files) {
			var file = files[key];
			var name = file.name;
			if (typeof file.fullPath !== 'undefined') {
				name = file.fullPath;
			}
			newFiles[name] = file;
		}
		for (var key in newFiles) {
			var file = newFiles[key];
			var name = file.name;
			if (typeof file.fullPath !== 'undefined') {
				name = file.fullPath;
			}

			var tr = document.createElement("tr");
			tr.setAttribute("id", "option-" + name);

			var td = document.createElement("td");
			td.innerHTML = name;
			var input = document.createElement("input");
			input.setAttribute("type", "hidden");
			input.setAttribute("name", "path-" + name);
			input.setAttribute("value", name);
			td.appendChild(input);
			tr.appendChild(td);

			var group = "";
			var splitPath = name.split("/");
			var filename = "";
			if (splitPath.length > 1) {
				group = splitPath[splitPath.length - 2];
				filename = splitPath[splitPath.length - 1].split(".")[0];
			} else {
				splitPath = name.split(".");
				if (splitPath.length > 0) {
					group = splitPath[0];
					filename = splitPath[0];
					if (filename.toLowerCase().match(/\d+/)) {
						group = getMovieName(parseInt(filename.toLowerCase().match(/\d+/)));
					}
				}
			}
			td = document.createElement("td");
			var input = document.createElement("input");
			input.setAttribute("name", "input-" + name);
			input.setAttribute("type", "text");
			input.setAttribute("value", group);
			td.appendChild(input);
			tr.appendChild(td);

			td = document.createElement("td");
			var div = document.createElement("div");
			for (var i = 0; i < 4; i++) {
				var radio = document.createElement("input");
				radio.setAttribute("class", "radio-" + i);
				radio.setAttribute("name", "radio-" + name);
				radio.setAttribute("type", "radio");
				radio.setAttribute("value", radioKey[i]);
				var label = document.createElement("label");
				label.setAttribute("for", radioKey[i]);
				label.setAttribute("i18n", radioKey[i]);
				setLocaleText(label);
				div.appendChild(radio);
				div.appendChild(label);
			}

			if (filename.toLowerCase().match("(^small)") || filename.toLowerCase().match("(^小图)")) {
				div.getElementsByClassName("radio-1")[0].checked = true;
			} else if (filename.toLowerCase().match("(c$)")) {
				div.getElementsByClassName("radio-2")[0].checked = true;
			} else if (filename.toLowerCase().match("(e$)")) {
				div.getElementsByClassName("radio-3")[0].checked = true;
			} else if (filename.toLowerCase().match("(^big)")) {
				div.getElementsByClassName("radio-2")[0].checked = true;
			}
			td.appendChild(div);
			tr.appendChild(td);

			fragment.appendChild(tr);
		}

		if (fragment.childNodes.length > 0) {
			var table = document.createElement("table");
			table.appendChild(fragment);
			document.getElementById("success-table").innerHTML = "";
			document.getElementById("success-table").appendChild(table);
			$("#success-popup").show();
		}
	}
}

function getMovieName(position) {
	var results = jQuery.grep(movieList, function (n, i) {
		return n["no"] == position;
	}, false);

	if (results.length > 0) {
		return results[0]["chi"];
	}
	return "";
}

function checkInput(string) {
	var input = string.split("\n");
	var fragment = document.createDocumentFragment();
	var itemNo = 1;
	var tempMovieList = [];
	$.each(input, function(k) {
		var text = input[k];
		var cols = text.split("\t");

		var i = 0;
		while (cols.length > i && cols[i] == '' + itemNo) {
			var tr = document.createElement("tr");

			var td = document.createElement("td");
			td.innerHTML = '' + itemNo;
			tr.appendChild(td);

			var nameEng = cols[++i];
			var nameChi = cols[++i];
			var imageSmall = '';
			var imageBigC = '';
			var imageBigE = '';

			if (isImage(cols[cols.length-1])) {
				imageSmall = cols[cols.length-3];
				imageBigC = cols[cols.length-2];
				imageBigE = cols[cols.length-1];
			}

			td = document.createElement("td");
			td.innerHTML = nameEng;
			tr.appendChild(td);

			td = document.createElement("td");
			td.innerHTML = nameChi;
			tr.appendChild(td);

			var hasSmall = checkServerImage("movies/" + nameChi + "/small.png");
			var hasBigC = checkServerImage("movies/" + nameChi + "/big_c.jpg");
			var hasBigE = checkServerImage("movies/" + nameChi + "/big_e.jpg");
			if (hasSmall || hasBigC || hasBigE) {
				td = document.createElement("td");
				var img = document.createElement("img")
				img.src = "movies/" + nameChi + "/small.png";
				img.height = 200;
				td.appendChild(img);
				tr.appendChild(td);

				td = document.createElement("td");
				img = document.createElement("img")
				img.src = "movies/" + nameChi + "/big_c.jpg";
				img.height = 200;
				td.appendChild(img);
				tr.appendChild(td);

				td = document.createElement("td");
				img = document.createElement("img")
				img.src = "movies/" + nameChi + "/big_e.jpg";
				img.height = 200;
				td.appendChild(img);
				tr.appendChild(td);
			}

			fragment.appendChild(tr);

			var movie = {
				no: itemNo,
				chi: nameChi,
				eng: nameEng
			}

			tempMovieList.push(movie);

			itemNo++;
			break;
		}
	});

	if (fragment.childNodes.length > 0) {
		var table = document.createElement("table");
		table.appendChild(fragment);
		document.getElementById("my-list").innerHTML = "";
		document.getElementById("my-list").appendChild(table);

		movieList = tempMovieList;
	}

	return input.length > 15;
}

function checkSave(data) {
	$("#filename-select").empty();
	var saveSelect = $("#filename-select")[0];
	$.each(data, function(k) {
		var saveObj = data[k];
		var option = document.createElement("option");
		var filename = saveObj.name;
		option.text = filename.split('.').slice(0, -1).join('.');
		option.value = filename;
		saveSelect.options.add(option);
	});
}

function isImage(file) {
	if (file.length > 4) {
		var ext = file.substring(file.length - 4);
		return ext == ".png" || ext == ".jpg";
	}
}

function checkServerImage(image) {
	var http = new XMLHttpRequest();

	http.open('HEAD', image, false);
	http.send();

	return http.status != 404;
}
